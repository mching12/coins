package com.example.coins.view

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.coins.data.CoinRepository
import com.example.coins.data.model.Coin
import com.example.coins.data.model.CoinType
import com.example.coins.data.model.Wallet
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class MainViewModel (
    private val coinRepository: CoinRepository
) : ViewModel() {

    private val _mainState = MutableLiveData<MainState>()
    private val compositeDisposable = CompositeDisposable()
    private val wallet1 = Wallet(CoinType.One)
    private val wallet5 = Wallet(CoinType.Five)
    private val wallet10 = Wallet(CoinType.Ten)
    private val wallet20 = Wallet(CoinType.Twenty)

    fun getMainState(): LiveData<MainState> = _mainState

    fun start() {
        coinRepository.run {
            poolCoins(getRandomCoins())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d(TAG, "coin list emitted: $it")
                    it.forEach { coin -> addToWallet(coin) }
                    _mainState.value = MainState.Data(getSortedWalletList())
                }, {
                    _mainState.value = MainState.Error(it.localizedMessage)
                })
        }
    }

    private fun getSortedWalletList() =
        listOf(wallet1, wallet5, wallet10, wallet20).sortedBy {
            it.coins.count()
        }.toMutableList()

    private fun addToWallet(coin: Coin) {
        when(coin.type) {
            CoinType.One -> wallet1.addCoin(coin)
            CoinType.Five -> wallet5.addCoin(coin)
            CoinType.Ten -> wallet10.addCoin(coin)
            CoinType.Twenty -> wallet20.addCoin(coin)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    companion object {
        private const val TAG = "MainViewModel"
    }
}