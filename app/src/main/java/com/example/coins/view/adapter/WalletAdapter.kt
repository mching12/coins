package com.example.coins.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.coins.R
import com.example.coins.data.model.Wallet
import kotlinx.android.synthetic.main.list_item_wallet.view.*

class WalletAdapter(
    private val context: Context
) : ListAdapter<Wallet, WalletAdapter.WalletVH>(WalletDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        WalletVH(context, LayoutInflater.from(context).inflate(R.layout.list_item_wallet, parent, false))

    override fun onBindViewHolder(holder: WalletVH, position: Int) {
        holder.bind(getItem(position))
    }

    override fun submitList(list: MutableList<Wallet>?) {
        super.submitList(list)
        notifyDataSetChanged()
    }

    class WalletVH(private val context: Context, private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(wallet: Wallet) {
            view.tvTitle.text = context.getString(R.string.wallet_title, wallet.coinType.name, wallet.coins.size)
        }
    }

    class WalletDiff: DiffUtil.ItemCallback<Wallet>() {
        override fun areItemsTheSame(oldItem: Wallet, newItem: Wallet): Boolean = (oldItem.coinType == newItem.coinType)
        override fun areContentsTheSame(oldItem: Wallet, newItem: Wallet) = when {
            oldItem.coins.size != newItem.coins.size -> false
            else -> {
                oldItem.coins.forEachIndexed { index, coin ->
                    if (newItem.coins[index].uuid != coin.uuid) return false
                }
                true
            }
        }
    }
}