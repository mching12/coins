package com.example.coins.view

import com.example.coins.data.model.Wallet

sealed class MainState {
    data class Data(val wallets: MutableList<Wallet>): MainState()
    data class Error(val error: String?): MainState()
}