package com.example.coins

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.coins.view.MainState
import com.example.coins.view.MainViewModel
import com.example.coins.view.adapter.WalletAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var walletAdapter: WalletAdapter

    //  PR2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initList()
        initVM()
    }

    private fun initList() {
        rvList.apply {
            walletAdapter =
                WalletAdapter(this@MainActivity)
            adapter = walletAdapter
        }
    }

    private fun initVM() {
        viewModel.getMainState().observe(this, Observer {
            renderLoginState(it)
        })
        viewModel.start()
    }

    private fun renderLoginState(state: MainState) {
        when(state) {
            is MainState.Data -> {
                state.wallets.forEach {
                    Log.d(this.javaClass.simpleName, "wallet ${it.coinType.name}: ${it.coins.size}")
                }
                walletAdapter.submitList(state.wallets)
            }
            is MainState.Error ->
                Toast.makeText(this, state.error, Toast.LENGTH_SHORT).show()
        }
    }
}
