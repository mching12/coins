package com.example.coins.data

import android.util.Log
import com.example.coins.data.model.Coin
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class CoinRepository {

    //  first goal create an observable that emits random values of coins every 100ms
    fun getRandomCoins(): Observable<Coin> =
        Observable.interval(INTERVAL_FIRST_GOAL, TimeUnit.MILLISECONDS)
            .flatMap {
                return@flatMap Observable.create<Coin> { emitter ->
                    val randomCoin = Coin.getRandom()
                    Log.d(TAG, "coin created: $randomCoin")
                    emitter.run {
                        onNext(randomCoin)
                        onComplete()
                    }
                }
            }

    //  second goal create an observable that will collect coins from bankObservable @2 second intervals
    //  the observable will emit all collected coin as a list or array of coins
    fun poolCoins(coin: Observable<Coin>): Observable<MutableList<Coin>> =
        coin.buffer(INTERVAL_SECOND_GOAL, TimeUnit.SECONDS)

    companion object {
        private const val TAG = "CoinRepo"
        private const val INTERVAL_FIRST_GOAL = 100L
        private const val INTERVAL_SECOND_GOAL = 2L
    }
}