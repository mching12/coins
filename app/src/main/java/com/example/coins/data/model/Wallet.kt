package com.example.coins.data.model

data class Wallet (
    val coinType: CoinType,
    val coins: ArrayList<Coin> = arrayListOf()
) {
    fun addCoin(coin: Coin) {
        coin.takeIf {
            it.type == coinType
        }?.run {
            coins.add(this)
        }
    }
}