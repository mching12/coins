package com.example.coins.data.model

enum class CoinType {
    One,
    Five,
    Ten,
    Twenty;

    companion object {
        fun getRandom() = values().toList().shuffled().first()
    }
}