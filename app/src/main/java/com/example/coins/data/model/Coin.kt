package com.example.coins.data.model

import java.util.*

data class Coin (
    val uuid: UUID = UUID.randomUUID(),
    val type: CoinType
) {
    companion object {
        fun getRandom() = Coin(type = CoinType.getRandom())
    }
}