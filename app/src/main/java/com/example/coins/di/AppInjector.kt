package com.example.coins.di

import com.example.coins.data.CoinRepository
import com.example.coins.view.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
}

val repositoryModule = module {
    single {
        CoinRepository()
    }
}